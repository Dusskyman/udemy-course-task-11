import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_task_11/providers/text_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: TextProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List<int> list = List.generate(101, (index) => index);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Consumer<TextProvider>(
              builder: (context, value, child) => Container(
                  padding: EdgeInsets.all(30),
                  alignment: Alignment.center,
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(100)),
                  child: Text(
                    value.text,
                    maxLines: 3,
                    style: TextStyle(fontSize: 20),
                  )),
            ),
            Padding(padding: EdgeInsets.all(20)),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: ColoredBox(
                color: Colors.grey,
                child: DropdownButton(
                  dropdownColor: Colors.grey,
                  items: [
                    ...list
                        .map((e) => DropdownMenuItem(
                            onTap: () {
                              Provider.of<TextProvider>(context, listen: false)
                                  .setText(e);
                            },
                            child: Text(e.toString())))
                        .toList()
                  ],
                  onChanged: (value) {
                    return value;
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
