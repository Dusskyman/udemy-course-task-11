import 'package:flutter/material.dart';

class TextProvider with ChangeNotifier {
  String _text = 'There is simple text';
  String get text {
    return _text;
  }

  void setText(int toString) {
    _text = toString.toString();
    notifyListeners();
  }
}
